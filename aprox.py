#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.
'''

import sys


def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):

    productions = []
    for prod in range(min_trees, max_trees+1):
        productions.append((prod, compute_trees(prod)))

    return productions



def read_arguments():
    if len(sys.argv) != 6:
        sys.exit(f"Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")

    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])

    except ValueError:
        sys.exit("All arguments must be integers")
    return base_trees, fruit_per_tree, reduction, min, max

def main():
    global base_trees, fruit_per_tree, reduction

    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)

    best_production = 0
    best_trees = 0

    for i in productions:
        print(i[0], i[1])
        if i[1] > best_production:
            best_production = i[1]
            best_trees = i[0]

    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()